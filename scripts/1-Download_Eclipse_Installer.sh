#!/bin/bash

# Variables used in commands
ECLIPSE_OPERATING_SYSTEM="linux64"
ECLIPSE_INSTALLER_ARCHIVE_FILE="eclipse-inst-${ECLIPSE_OPERATING_SYSTEM}.tar.gz"
ECLIPSE_INSTALLER_DOWNLOAD_URL="https://www.eclipse.org/downloads/download.php?file=/oomph/products/${ECLIPSE_INSTALLER_ARCHIVE_FILE}&r=1"

# Download the installer with WGET
echo "Downloading from '${ECLIPSE_INSTALLER_DOWNLOAD_URL}'"
wget --max-redirect=10 --output-document="${ECLIPSE_INSTALLER_ARCHIVE_FILE}" "${ECLIPSE_INSTALLER_DOWNLOAD_URL}"

# Unpack the downloaded installer and clean up download file
tar --extract --warning=no-unknown-keyword --file=${ECLIPSE_INSTALLER_ARCHIVE_FILE}
rm ${ECLIPSE_INSTALLER_ARCHIVE_FILE}

# Show folder changes
pwd
ls -1sh