# MDE-DevOps Example

## How to build a Docker Image containing Epsilon

This is a GitLab project for creating a Docker image with Eclipse and Epsilon installed. This container runs a full Eclipse-IDE using an X11 virtual frame buffer just incase GUI components check for a desktop environment. Ant build files are used to script the execution of model management programs.

## Files

**.gitlab-ci.yml**, the CI pipeline script for building the docker container using kaniko.

**Dockerfile**, docker build file that kaniko uses to create the container image.

## Folders

**scripts**, bash scripts for installing Eclipse and Epsilon (1,2). An entry point script for the container (3).

**epsilon_playground-example**, one of the epsilon playground examples to test the container can execute epsilon programs.
